
# Task 1
quality <- airquality
ggplot(quality, aes(x = Month, y = Ozone, group = Month))+
  geom_boxplot()

# Task 2
?aes

boo <- mtcars
plot1 <- ggplot(mtcars, aes(x = mpg, y = disp, fill = hp))+
  geom_point()

?iris
ggplot(iris, aes(Sepal.Length)) + geom_histogram(aes(fill = Species))
ggplot(iris, aes(Sepal.Length, fill = Species)) + geom_histogram()

ggplot(iris, aes(Sepal.Length, Sepal.Width, col = Species, size = Petal.Length)) +
  geom_point()